# .NET MVC Test #

Please create a simple web application with these following requirements:

* An age gate to verify the user must be 18 or older to progress to the next page. It is okay to use just a simple text field with regex validation on both client and server sides.

* The next page will be a categories and products listing page, maybe some alcohol or beers. Product data will be loaded from an Xml.

* A couple sorting functions on product listing page such as Highest to lowest price, Lowest to highest price